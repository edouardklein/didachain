(in-package :cl-user)
(defpackage didachain.web
  (:use :cl
        :caveman2
        :didachain.config
        :didachain.view
        :didachain.db
        :datafly
        :sxql)
  (:export :*web*))
(in-package :didachain.web)

;; for @route annotation
(syntax:use-syntax :annot)

;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)
;; A name->uuid map, to prevent double logins.
(defvar *login-table* (make-hash-table :test 'equal))
(setf *login-table* (make-hash-table :test 'equal))

;; Blockchain rules:
;; Gen 0, only one rule:
;; Block id -1 must exists => gen0-previousp
;; Gen 0 has only one rule, and this rule in mostly incompatible
;; with the rest, so we handle gen0 in with its own set of functions
;; instead of making a single set, that would have to be compatible
;; with both gen0 and gen>0.
;; The incompatibility stems from the previous block number
;; going from an integer in gen0 to a hash hexdigest string in gen>0.
(defvar *gen0-blockchain* '())
(setf *gen0-blockchain*
      '((:previous -1 :src "God" :amount 10 :dst "alice" :hash 0)
        (:previous 0 :src "God" :amount 10 :dst "bob" :hash 1)
        (:previous 1 :src "God" :amount 10 :dst "charlie" :hash 2)
        (:previous 2 :src "God" :amount 10 :dst "dave" :hash 3)
        (:previous 3 :src "God" :amount 10 :dst "eve" :hash 4)
        (:previous 4 :src "God" :amount 10 :dst "fabio" :hash 5)
        (:previous 6 :src "God" :amount 10 :dst "geralt" :hash 6)
        (:previous 6 :src "God" :amount 10 :dst "hector" :hash 7)
        (:previous 7 :src "God" :amount 10 :dst "isidore" :hash 8)
        (:previous 8 :src "God" :amount 10 :dst "juliet" :hash 9)
        ))

(defun gen0-previousc (blck)
  "Check that blocknb has a predecessor in the blockchain"
  (unless (loop for block in *gen0-blockchain*
                when (= (getf block :hash) (getf blck :previous))
                  collect block)
    (error (format nil "Can't insert block nb ~A because there is no known block ~A"
                   (getf blck :hash) (getf blck :previous)))))

(defun gen0-save-block (blck)
  "Actually put the block in the blockchain"
  (when (= (getf blck :previous)
         (getf blck :hash))
      (error (format nil "A block can't be its own parent")))
  (push blck *gen0-blockchain*))

(defun gen0-make-block (author uuid current src amount dst)
  "Return a plist containing the parsed information from the arguments"
  (let* ((unprevioused-block
           `(:author ,(string author)
             :uuid ,(string uuid)
             :hash ,(parse-integer current)
             :src ,(string src)
             :amount ,(parse-integer amount)
             :dst ,(string dst)))
         (previous (1- (getf unprevioused-block :hash))))
    (append unprevioused-block `(:previous ,previous))))

(defun gen0-block-id (blck)
  "Return a string that uniquely identifies blck, to be used as its id in the dot file"
  ;; Starts with TOTO because it can't start with a number
  (format nil "TOTO~A~A~A~A"
          (getf blck :hash)
          (getf blck :src)
          (getf blck :amount)
          (getf blck :dst)))

(defparameter *metasyntactic-name* "toto")
(defun gen0-dump-blockchain-to-dot ()
  "Save the blockchain in a /tmp/toto.dot using the braindead gen0 format where
block names are sequential numbers and a block can have multiple parents."
  (with-open-file (s (format nil "/tmp/~a.dot" *metasyntactic-name*)
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (format s "digraph G {~%")
    (loop for block in *gen0-blockchain*
          do (format s "~A [shape=box3d,label=\"~A ~A -[~A]-> ~A\"];~%"
                     (gen0-block-id block)
                     (getf block :hash)
                     (getf block :src)
                     (getf block :amount)
                     (getf block :dst)))
    (loop for block in *gen0-blockchain*
          do (loop for previous-block in *gen0-blockchain*
                   when (= (getf previous-block :hash)
                           (getf block :previous))
                     do (format s "~A -> ~A ;~%"
                                (gen0-block-id previous-block)
                                (gen0-block-id block))))
    (format s "}~%")))

;; The next rule must be:
;; Gen 1
;; Block ID must be the hash of an existing block => previousc
;; Here is the code to handle a non-braindead blockchain
;; The list of known blocks. Initialized so that every team has 10 coins
(defvar *blockchain* '())
(setf *blockchain*
      '((:previous "JUST NO" :src "God" :amount 10 :dst "alice" :hash "dead")
        (:previous "dead" :src "God" :amount 10 :dst "bob" :hash "beef")
        (:previous "beef" :src "God" :amount 10 :dst "charlie" :hash "defe")
        (:previous "defe" :src "God" :amount 10 :dst "dave" :hash "c8ed")
        (:previous "c8ed" :src "God" :amount 10 :dst "eve" :hash "cafe")
        (:previous "cafe" :src "God" :amount 10 :dst "fabio" :hash "fade")
        (:previous "fade" :src "God" :amount 10 :dst "geralt" :hash "face")
        (:previous "face" :src "God" :amount 10 :dst "hector" :hash "feed")
        (:previous "deaf" :src "God" :amount 10 :dst "isidore" :hash "deaf")
        (:previous "babe" :src "God" :amount 10 :dst "juliet" :hash "babe")
        ))

(defun dump-blockchain-to-dot ()
  "Save the blockchain in a /tmp/toto.dot"
  (with-open-file (s (format nil "/tmp/~a.dot" *metasyntactic-name*)
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (format s "digraph G {~%")
    (loop for block in *blockchain*
          do (format s "~A [shape=box3d, label=\"~A ~A -[~A]-> ~A\"];~%"
                     (getf block :hash)
                     (getf block :hash)
                     (getf block :src)
                     (getf block :amount)
                     (getf block :dst)))
    (loop for block in *blockchain*
          do (loop for previous-block in *blockchain*
                   when (string= (getf previous-block :hash)
                           (getf block :previous))
                     do (format s "~A -> ~A ;~%"
                                (getf previous-block :hash)
                                (getf block :hash))))
    (format s "}~%")))

(defun save-block (blck)
  "Actually put the block in the blockchain"
  (when (string= (getf blck :previous)
                 (getf blck :hash))
    (error "A block can't be its own parent"))
  (when (by-hash (getf blck :hash))
    (error "This block is already in the blockchain"))
  (push blck *blockchain*))

(defun sha-256 (obj)
  "https://rosettacode.org/wiki/SHA-256#Common_Lisp"
  (ironclad:byte-array-to-hex-string
    (ironclad:digest-sequence :sha256
                              (ironclad:ascii-string-to-byte-array (format nil "~A" obj)))))

(defun make-block (author uuid blocknb src amount dst)
  "Return a plist containing the parsed information from the arguments"
  (let* ((unhashed-block
           `(:author ,(string author)
             :uuid ,(string uuid)
             :previous ,(string blocknb)
             :src ,(string src)
             :amount ,(parse-integer amount)
             :dst ,(string dst)))
         ;; The hash must start with an alphabetic character to be a valid DOT id
         (hash (concatenate 'string "x" (subseq (sha-256 unhashed-block) 0 5))))
    (append unhashed-block `(:hash ,hash))))

;; For any subsequent generation, activate the following rules
;; one after the other, depending on which attack is successful
;; Amount must be a positive number => positive-amountc
;; The longest chain is where the consensus is
;; The block must be signed => signedc
;; The transaction must make financial sense => balancec
;; One must provide a proof of work => simulate-powc

(defun signedc (blck)
  "Check that the author of the transaction is the logged in user"
  (unless (equal (getf blck :author)
                 (getf blck :src))
    (error (format nil "Block is signed by ~A but tries to spend funds belonging to ~A"
                   (getf blck :author)
                   (getf blck :src)))))

(defun by-hash (h)
  "Return the block in the blockchain whose hash is h"
  (loop for block in *blockchain*
        when (string= h (getf block :hash))
          return block))

(defun previous (blck)
  "Return the block whose hash is blck's :previous"
  (by-hash (getf blck :previous)))

(defun chain (blck)
  "Return the chain of block in chronological order from the genesis to blck"
  (reverse
   (loop for block = blck then (previous block)
         while block
         collect block)))

(defun apply-transaction (balance block)
  "Apply the transaction in block to the current balance, return the new balance"
  (let* ((src (intern (getf block :src)))
         (dst (intern (getf block :dst)))
         (amount (getf block :amount))
         (src-balance (getf balance src 0))
         (src-new-balance (- src-balance amount))
         (dst-balance (getf balance dst 0))
         (dst-new-balance (+ dst-balance amount)))
    (when (and (minusp src-new-balance) (not (string= src "God")))
      (error "Insuficient funds in ~A's wallet (~A) to transfer ~A out of it"
             src src-balance amount))
    (when (string= src dst)
      (return-from apply-transaction balance))
    (setf (getf balance dst) dst-new-balance)
    (setf (getf balance src) src-new-balance)
    balance))

(defun rbalance (balance chain)
  "Recursive computation of the balance"
  (if (not chain)
      balance
      (rbalance (apply-transaction balance (pop chain)) chain)))

(defun balance (chain)
  "Return the final balance for the given chain"
  (rbalance '() chain))

(defun balance-from-hash (hash)
  "Return the balance from a hash"
  (balance (chain (loop for blck in *blockchain*
                        if (string= (getf blck :hash) hash)
                          return blck))))

(defun balancec (blck)
  "Check that we can compute the balance if this block was part of the chain"
  (balance (chain blck)))

(defun previousc (blck)
  "Check that blocknb has a predecessor in the blockchain"
  (unless (previous blck)
    (error (format nil "Can't insert block because there is no known block ~A"
                   (getf blck :previous)))))

(defun positive-amountc (blck)
  "Check that amount is positive"
  (unless (plusp (getf blck :amount))
    (error (format nil "Can't transfer a non positive amount"))))


(defparameter *cooldown-table* '())
(setf *cooldown-table* '())
(defparameter *cooldown-period* 60)  ; seconds

(defun elapsed-since (then)
  "Return the number of seconds between now and t"
  (- (get-universal-time) then))

(defun simulate-powc (blck)
  "Check that a cooldown period has passed since we last received a block from
blck's author. This simulates proof-of-work, as a single author can no longer spam the
 blockchain, but it is less than ideal for more technical students, who should be able
to see all the subtleties of PoW by implementing the hash search themselves."
  (let* ((author (intern (getf blck :author)))
         (last-time (getf *cooldown-table* author)))
    (format t "in powc last time is ~A~%" last-time)
    (when (and last-time
               (< (elapsed-since last-time) *cooldown-period*))
      (error "Too soon for another block"))
    (setf (getf *cooldown-table* author) (get-universal-time))))

;;
;; Routing rules

(defroute "/" ()
  (render #P"login.html"))

(defroute ("/login" :method :POST) (&key |team|)
  (let ((team (string |team|)))
    (if (gethash team *login-table*)
        (throw-code 403)
        (setf (gethash team *login-table*) (format nil "~A" (uuid:make-v4-uuid))))
    (let ((uuid (gethash team *login-table*)))
      (render #P"gen_0.html" `(:uuid ,uuid :author ,team)))))

(defun loggedinc (blck)
  "Check that the author of the block is correctly logged in"
  (unless (equal (getf blck :uuid)
                 (gethash (getf blck :author) *login-table*))
    (error (format nil "Invalid login for author ~A with uuid ~A"
                   (getf blck :author)
                   (getf blck :uuid)))))

(defroute ("/new" :method :POST) (&key |author| |uuid| |blocknb| |src| |amount| |dst|)
  (let ((blck (gen0-make-block |author| |uuid| |blocknb| |src| |amount| |dst|)))
  ;;(let ((blck (make-block |author| |uuid| |blocknb| |src| |amount| |dst|)))
    (loop for check in '(loggedinc
                         gen0-previousc
                         ;;previousc
                         ;;positive-amountc
                         ;;signedc
                         ;;balancec
                         ;;simulate-powc
                         gen0-save-block)
                         ;;save-block)
          do (handler-case
                 (funcall check blck)
               (error (c) (return (format nil "Rejected block: ~A: ~A" check c))))
          finally (return (render  #P"gen_0.html" `(:uuid ,(getf blck :uuid)
                                                    :author ,(getf blck :author)))))))

(defroute ("/blockchain.svg") ()
  (setf (getf (response-headers *response*) :content-type) "image/svg+xml")
  ;;(dump-blockchain-to-dot)
  (gen0-dump-blockchain-to-dot)
  (uiop:run-program (format nil "dot -T svg < /tmp/~a.dot > /tmp/~a.svg"
                            *metasyntactic-name* *metasyntactic-name*))
  (uiop:read-file-string (format nil "/tmp/~a.svg" *metasyntactic-name*)))

(defroute ("/blockchain") ()
  (render #P"blockchain.html"))


;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
