(use-modules (guix packages))
(use-modules (guix utils))
(use-modules (guix gexp))
(use-modules (gnu packages))
(use-modules (gnu packages lisp))
(use-modules (gnu packages lisp-xyz))
(use-modules (gnu packages readline))
(use-modules (gnu packages graphviz))
(use-modules (guix profiles))
(use-modules (guix download))
(use-modules (guix build-system trivial))
(use-modules (guix build-system copy))
(use-modules (guix licenses))
(use-modules (guix git-download))
(use-modules (gnu packages python))
(use-modules ((guix licenses) #:prefix license:))
(use-modules (ice-9 popen))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define (skip-git-and-build-directory file stat)
  "Skip the `.git` and `build` and `guix_profile` directory when collecting the sources."
  (let ((name (basename file)))
    (not (or (string=? name ".git")
             (string=? name "build")
             (string-prefix? "guix_profile" name)))))

(define *version* (call-with-input-file "VERSION"
                    get-string-all))

(define-public didachain
  (package
   (name "didachain")
   (version (git-version *version* "HEAD" %git-commit))
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? skip-git-and-build-directory))
   ;; (native-inputs `(
   ;;                  ("FIXME" ,FIXME)
   ;;                  ))
   ;;(propagated-inputs `(
   ;;           ("FIXME" ,FIXME)
   ;;                     ("" ,)
   ;;                     ))
   (inputs (list sbcl sbcl-caveman rlwrap sbcl-slime-swank cl-asdf sbcl-ningle
                 sbcl-clack sbcl-lack sbcl-envy sbcl-cl-ppcre sbcl-djula sbcl-datafly sbcl-sxql
                 sbcl-uuid graphviz
))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan '(
                       ("./" "bin/")
                       )
      #:phases
      (modify-phases %standard-phases
                     (add-before
                      'build 'check
                      (lambda* (#:key outputs #:allow-other-keys)
                               (let* ((dir (string-append
                                            (assoc-ref outputs "out")
                                            "/bin/")))
                                 (chdir dir)
                                 (invoke "make" "test")
                                 #t))))
      ))
   (synopsis "Later")
   (description
    "Later")
   (home-page "Later")
   (license license:agpl3+)))

didachain
