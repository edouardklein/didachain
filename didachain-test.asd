(defsystem "didachain-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Edouard Klein"
  :license ""
  :depends-on ("didachain"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "didachain"))))
  :description "Test system for didachain"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
