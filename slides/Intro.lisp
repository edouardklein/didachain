(in-package :cl-smt-lib)

(slideshow
 (slide
  (title "Blockchain et procédure pénale")
  (image "scale.png")
  (url "https://rdklein.fr")
  (url "edou@rdklein.fr"))
 (slide
  (title "Blockchain et procédure pénale")
  (bullet "Registre distribué infalsifiable")
  (bullet "Clef/Adresse/Cluster/Wallet")
  (bullet "Données/Monnaie/Actif/Contrat")
  (bullet "Traçage, Inventaire, Saisie")
  (bullet "Echangeurs, Mixers, Vendeurs")
  (image "mishmash.png"))
 (slide
  (title "Histoire de la cryptographie")
  (image "timeline.png")
  (bullet "Le monde change en 1977"))
 (slide
  (title "Primitives cryptographiques")
  (bullet "Chiffrement")
  (bullet "Hash")
  (bullet "Signature")
  (image "sealed.png"))
 (slide
  (title "Les généraux byzantins")
  (image "byzance.png")
  (bullet "1982"))
 (slide
  (title "Bitcoin")
  (image "bitcoin.png")
  (bullet "2009"))
 (slide
  (title "Règles du jeu")
  (image "game.png")
  (bullet "Donner 2 sous à Ted")
  (bullet "Garder ses 8 autres sous")
  (bullet "(Voler/générer des sous)")
  (bullet "(Attaquer les autres)")
  (bullet "Respecter le protocole")
  (bullet "Pas d'attaques physiques")))
